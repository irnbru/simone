import QtQuick 2.7
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3

Page {
  id: settingsPage

  // sliderValue is for volume ( should probably rename it )
  property alias sliderValue: slider.value
  property alias speedValue: speedSlider.value
  property int curIndex: 0

  Settings {
    id: curInd
    property alias curIndex: settingsPage.curIndex
  }

  header: PageHeader { title: 'Sound' }
  //header: PageHeader { title: root.soundType }

  ListModel {
    id: model
    ListElement { instrument: 'Piano' }
    ListElement { instrument: 'Drum' }
    ListElement { instrument: 'Beep' }
    // ListElement { instrument: 'Farm' }
  }

  Column {
    anchors { top: parent.header.bottom; bottom: parent.bottom }
    width: parent.width

    ListItem {
      id: sequenceSpeed
      height: lilSpeed.height + speedSlider.height + divider.height
      ListItemLayout {
        id: lilSpeed
        title.text: 'Speed'
      }
      Slider {
        id: speedSlider
        anchors {
          top: lilSpeed.bottom
          left: parent.left
          leftMargin: units.gu(2)
          right: parent.right
          rightMargin: units.gu(2)
        }
        minimumValue: 0
        maximumValue: 20
        stepSize: 1
        onValueChanged: {
          formatValue(value)
          root.sequenceSpeed = value
        }
      }
    }

    ListItem {
      id: volumeSlider
      height: lilVol.height + slider.height + divider.height
      ListItemLayout {
        id: lilVol
        title.text: 'Volume'
      }
      Slider {
        id: slider
        anchors {
          top: lilVol.bottom
          left: parent.left
          leftMargin: units.gu(2)
          right: parent.right
          rightMargin: units.gu(2)
        }
        minimumValue: 0
        maximumValue: 100
        stepSize: 1
        onValueChanged: {
          formatValue(value)
          root.volume = value / 100
        }
      }
    }

    ListView {
      id: listview
      height: parent.height - volumeSlider.height
      width: parent.width
      clip: true
      currentIndex: curIndex
      model: model
      delegate: ListItem {
        height: lil.height + divider.height
        ListItemLayout {
          id: lil
          title.text: instrument
          height: tickIcon.height * 3

          Icon {
            id: tickIcon
            name: 'tick'
            width: height
            height: units.gu(2)
            visible: index === listview.currentIndex
          }
        }
        onClicked: {
          listview.currentIndex = index
          root.soundType = instrument
          curIndex = index
        }
      }
    }
  }
}
