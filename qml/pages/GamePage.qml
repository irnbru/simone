import QtQuick 2.7
import Ubuntu.Components 1.3
import '../components'

Page {
  id: gamePage

  property var options: [yellow, blue, red, green]
  property alias sounds: grid.sounds
  property var sequence: []
  property var playerSequence: []
  property int sequenceIndex: 0
  property bool btnActive: false
  property int round: 1
  //set volume from menu page
  property alias volume: grid.volume

  header: PageHeader {
    title: i18n.tr('Simone')
  }

  function addToSequence() {
    var randomColor = options[Math.floor(Math.random() * options.length)]
    sequence.push(randomColor)
  }

  function restartGame() {
    restartBtn.visible = true
    sequence = []
    playerSequence = []
    //move to onClicked of restart btn
    //round = 1
    sequenceTimer.interval = 400 - (root.sequenceSpeed * 10)
  }

  function playerPress(btn) {
    if(!btnActive) return
    if(sequenceTimer.running) return

    btn.activateBtn()
    playerSequence.push(btn)

    if(playerSequence[playerSequence.length - 1] !== sequence[playerSequence.length - 1]) {
      message.text = 'Next button was ' + sequence[playerSequence.length - 1].btnColor
      btnActive = false
      setHighscore()
      restartGame()
    } else {
      // round completed
      if(playerSequence.length === sequence.length) {
        btnActive = false
        playerSequence = []
        round++
        if(sequenceTimer.interval > 200) sequenceTimer.interval -= 10
        nextRound(delayTimer)
      }
    }
  }

  function nextRound(timer) {
    addToSequence()
    timer.start()
  }

  function setHighscore() {
    if(gamePage.round <= root.highscore) return
    root.highscore = gamePage.round
  }

  Text {
    id: message
    text: 'Ready ?'
    color: '#fff'
    font.pixelSize: units.gu(3)
    anchors {
      horizontalCenter: parent.horizontalCenter
      bottom: grid.top
      bottomMargin: units.gu(6)
    }
  }

  // Current level
  Text {
    id: level
    color: '#fff'
    opacity: 0.1
    anchors.right: parent.right
    anchors.bottom: parent.bottom
    text: round
    font.pixelSize: units.gu(12)
  }

  Grid {
    property real volume
    property string sounds

    id: grid
    columns: root.width > root.height ? 4 : 2
    spacing: units.gu(2)
    anchors.centerIn: parent

    GameButton {
      id: yellow
      btnColor: 'yellow'
      onActivated: playerPress(yellow)
      sound: '../../assets/' + sounds + '/yellow.wav'
    }
    GameButton {
      id: blue
      btnColor: 'blue'
      onActivated: playerPress(blue)
      sound: '../../assets/' + sounds + '/blue.wav'
    }
    GameButton {
      id: red
      btnColor: 'red'
      onActivated: playerPress(red)
      sound: '../../assets/' + sounds + '/red.wav'
    }
    GameButton {
      id: green
      btnColor: 'green'
      onActivated: playerPress(green)
      sound: '../../assets/' + sounds + '/green.wav'
    }
  }

  Button {
    id: restartBtn
    visible: false
    color: UbuntuColors.inkstone
    anchors {
      horizontalCenter: parent.horizontalCenter
      top: grid.bottom
      topMargin: units.gu(8)
    }
    text: 'try again'
    // iconName: 'reload'
    onClicked: {
      round = 1
      nextRound(delayTimer)
      visible = false
      message.text = ''
      //======================NOT HERE====================================
      //btnActive = true
    }
  }

  // Loop through sequence and play each btn
  Timer {
    id: sequenceTimer
    running: false
    // interval: 400
    interval: 400 - (root.sequenceSpeed * 10)
    repeat: true
    onTriggered: {
      message.text = ''
      if(sequenceIndex >= sequence.length) {
        sequenceTimer.stop()
        sequenceIndex = 0
        btnActive = true
      } else {
        sequence[sequenceIndex].activateBtn()
        sequenceIndex++
      }
    }
  }

  // Stop the next round from starting immediately
  Timer {
    id: delayTimer
    running: false
    interval: 1000
    repeat: false
    onTriggered: sequenceTimer.start()
  }

  Component.onCompleted: {
    nextRound(delayTimer)
  }
}
