import QtQuick 2.7
import QtMultimedia 5.0
import Ubuntu.Components 1.3

Rectangle {
  property alias btnColor: btn.borderColor
  property string borderColor
  property alias sound: effect.source
  property real soundLevel: grid.volume
  property int btnSize: root.width > root.height ? 14 : 18
  //property int btnDuration: Math.floor((sequenceTimer.interval / 2) - 50)
  property int btnDuration: 75

  signal activated()

  // This plays the sequence
  function activateBtn() {
    effect.play()
    btnFlash.running = true
  }

  id: btn
  width: units.gu(btnSize)
  height: width
  color: 'transparent'
  radius: width
  border.width: units.gu(0.3)
  border.color: borderColor

  Rectangle {
    id: btnInner
    anchors.fill: parent
    radius: parent.width
    color: borderColor
    opacity: 0.2

    SequentialAnimation {
      id: btnFlash
      running: false
      NumberAnimation { target: btnInner; property: 'opacity'; to: 1.0; duration: btnDuration }
      NumberAnimation { target: btnInner; property: 'opacity'; to: 0.2; duration: btnDuration }
    }
  }

  SoundEffect {
    id: effect
    volume: soundLevel
  }

  MouseArea {
    anchors.fill: parent
    onClicked: btn.activated()
  }
}
