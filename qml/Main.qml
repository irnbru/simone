/*
 * Copyright (C) 2020  mills
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * simone is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Components.Popups 1.3

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'simone.mills'
    automaticOrientation: true
    backgroundColor: '#222'
    width: units.gu(45)
    height: units.gu(75)

    property int highscore: 0
    //property int volume: 50
    property real volume: 0.5
    //property int defaultVolume: 50
    property real defaultVolume: 0.5
    property string soundType: 'Piano'
    property int sequenceSpeed: 0
    // props for changing all styles easily
    property int btnSize: width > height ? units.gu(14) : units.gu(18)
    property double borderWidth: units.gu(0.3)
    property int iconSize: units.gu(6)
    property double opac: 0.2

    Settings {
      id: userSettings
      property alias userVolume: root.volume
      property alias soundType: root.soundType
      property alias highscore: root.highscore
      property alias sequenceSpeed: root.sequenceSpeed
    }

    // function convertVolume() {
    //   return volume / 100
    // }

    PageStack {
      id: stack
      Component.onCompleted: push(menuPage)

      Page {
        id: menuPage
        visible: false
        header: PageHeader {
          title: i18n.tr('Simone')
        }

        Grid {
          id: menuGrid
          anchors.centerIn: parent
          columns: 2
          spacing: units.gu(2)

          Rectangle {
            id: yellowScore
            width: btnSize
            height: width
            radius: width
            border.width: borderWidth
            border.color: 'yellow'
            color: 'transparent'

            Rectangle {
              id: yellowInner
              anchors.fill: parent
              radius: parent.width
              color: 'yellow'
              opacity: opac
            }

            Text { color: '#fff'; text: highscore; font.pixelSize: iconSize; anchors.centerIn: parent }

            MouseArea {
              anchors.fill: parent
              onClicked: PopupUtils.open(resetScoreDialog)
            }
          }

          Rectangle {
            id: blueSettings
            width: btnSize
            height: width
            radius: width
            border.width: borderWidth
            border.color: 'blue'
            color: 'transparent'

            Rectangle {
              id: blueInner
              anchors.fill: parent
              radius: width
              color: 'blue'
              opacity: opac
            }

            Icon {
              name: 'stock_music'
              color: '#fff'
              width: iconSize
              height: width
              anchors.centerIn: parent
            }

            MouseArea {
              anchors.fill: parent
              //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////volume
              onClicked: stack.push(Qt.resolvedUrl('pages/SettingsPage.qml'), { sliderValue: volume * 100, speedValue: sequenceSpeed })
            }
          }

          Rectangle {
            id: redMute
            width: btnSize
            height: width
            radius: width
            border.width: borderWidth
            border.color: 'red'
            color: 'transparent'

            Rectangle {
              id: redInner
              anchors.fill: parent
              radius: width
              color: 'red'
              opacity: opac
            }

            Icon {
              name: volume > 0 ? 'audio-volume-high' : 'audio-volume-muted'
              color: '#fff'
              width: iconSize
              height: width
              anchors.centerIn: parent
            }

            MouseArea {
              anchors.fill: parent
              onClicked: {
                volume > 0 ? volume = 0 : volume = defaultVolume
              }
            }
          }

          Rectangle {
            id: greenPlay
            width: btnSize
            height: width
            radius: width
            border.width: borderWidth
            border.color: 'green'
            color: 'transparent'

            Rectangle {
              id: greenInner
              anchors.fill: parent
              radius: width
              color: 'green'
              opacity: opac
            }

            Icon {
              name: 'toolkit_arrow-right'
              color: '#fff'
              width: iconSize
              height: width
              anchors.centerIn: parent
            }

            MouseArea {
              anchors.fill: parent
              onClicked: stack.push(Qt.resolvedUrl('pages/GamePage.qml'), { volume: volume, sounds: soundType })
            }
          }
        }
      }
    }

    Component {
      id: resetScoreDialog
      Dialog {
        id: resetScoreDialogue
        title: 'Reset Score'
        text: 'This will delete your highscore'
        Button {
          text: 'cancel'
          onClicked: PopupUtils.close(resetScoreDialogue)
        }
        Button {
          text: 'confirm'
          color: UbuntuColors.red
          onClicked: {
            root.highscore = 0
            PopupUtils.close(resetScoreDialogue)
          }
        }
      }
    }

}
